# create the given image for sbuild
define sbuild::qemu::image(
  String[1] $suite = $name,
  Optional[String[1]] $arch = $facts['os']['architecture'],
  Optional[String[1]] $path = "${sbuild::image_directory}/${suite}-autopkgtest-${arch}.img",
  Optional[String[1]] $mirror = 'https://deb.debian.org/debian',
) {
  include sbuild::qemu

  exec { "sbuild-qemu-create-${path}":
    command => "/usr/bin/sbuild-qemu-create --arch=${arch} -o=${path} ${suite} ${mirror}",
    creates => $path,
    require => [
      Package['sbuild-qemu'],
      File[dirname($path)],
    ],
    # one hour instead of default 5 minutes
    timeout => 3600,
  }
  file { $path:
    require => Exec["sbuild-qemu-create-${path}"],
    mode    => '0444',
  }
}
