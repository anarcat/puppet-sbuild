# create a tarball with mmdebstrap
define sbuild::unshare::tarball (
  String[1] $suite = $name,
  Optional[String[1]] $arch = $facts['os']['architecture'],
  Optional[String[1]] $path = "${sbuild::tarball_directory}/${suite}-${arch}.tar.zst",
  Optional[String[1]] $mirror = 'https://deb.debian.org/debian',
) {
  include sbuild::unshare

  exec { "sbuild-mmdebstrap-${path}":
    command => "mmdebstrap --include=ca-certificates --skip=output/dev --variant=buildd --arch=${arch} ${suite} ${path} https://deb.debian.org/debian",
    creates => $path,
    require => [
      Package['mmdebstrap'],
      File[dirname($path)],
    ],
    # one hour instead of default 5 minutes
    timeout => 3600,
  }
  file { $path:
    require => Exec["sbuild-mmdebstrap-${path}"],
    mode    => '0444',
  }
}
