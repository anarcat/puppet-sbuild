# setup sbuild
#
# this would ideall setup chroots and autopkgtest and qemu and who
# knows, but for now, i'm just layering the bits that i added from now
# on.
class sbuild(
  Boolean $manage_upgrades = false,
) {
  package { 'sbuild':
    ensure => present,
  }
  file { '/usr/local/sbin/sbuild-update-all':
    ensure  => bool2str($manage_upgrades, 'present', 'absent'),
    source  => 'puppet:///modules/sbuild/sbuild-update-all',
    mode    => '0555',
    require => Package['sbuild'],
  }
  systemd::unit_file { 'sbuild-update-all.service':
    ensure  => bool2str($manage_upgrades, 'present', 'absent'),
    source  => 'puppet:///modules/sbuild/sbuild-update-all.service',
    enable  => false,
    require => File['/usr/local/sbin/sbuild-update-all'],
  }
  systemd::unit_file { 'sbuild-update-all.timer':
    ensure  => bool2str($manage_upgrades, 'present', 'absent'),
    source  => 'puppet:///modules/sbuild/sbuild-update-all.timer',
    enable  => $manage_upgrades,
    active  => $manage_upgrades,
    require => File['/usr/local/sbin/sbuild-update-all'],
  }
}
