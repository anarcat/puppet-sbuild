# configure a qemu sbuilder
class sbuild::qemu (
  String[1] $image_directory = '/srv/sbuild/qemu',
  Hash[String, Hash] $images = { 'unstable' => {}, },
) {
  include sbuild

  package { 'sbuild-qemu':
    ensure => 'installed',
  }
  ensure_resource(
    'file',
    extlib::dir_split(dirname($image_directory)),
    {'ensure' => 'directory'}
  )

  file { $image_directory:
    ensure  => directory,
    purge   => true,
    force   => true,
    recurse => true,
    backup  => false, # possibly huge
  }

  $images.each |$image, $values| {
    sbuild::qemu::image { $image: * => $values }
  }
}
