# configure a set of tarballs for unshare mode
class sbuild::unshare (
  Hash[String, Hash] $tarballs = {},
) {
  include sbuild
  package { ['mmdebstrap', 'uidmap']:
    ensure => 'installed',
  }
  $tarballs.each | $tarball, $values | {
    sbuild::unshare::tarball { $tarball: * => $values }
  }
}
